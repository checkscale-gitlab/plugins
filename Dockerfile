FROM alpine:3.16.0@sha256:686d8c9dfa6f3ccfc8230bc3178d23f84eeaf7e457f36f271ab1acc53015037c as builder
# hadolint ignore=DL3018,DL3019
RUN set -eux; \
        apk add alpine-sdk sudo; \
        abuild-keygen -a -i -n;
COPY / /abuild
WORKDIR /abuild
RUN set -eux; \
        abuild -F -r -P /abuild; \
        find . -name '*.apk' -type f -exec apk add --no-cache {} \;;

FROM scratch
COPY --from=builder /var/www/html/plugins/ /var/www/html/plugins
